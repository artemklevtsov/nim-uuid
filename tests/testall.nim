import std/unittest
import std/strutils
import uuid

proc validChars(s: string): bool =
  const chars = {'-', '0' .. '9', 'a' .. 'f'}
  result = true
  for c in s:
    if c notin chars:
      return false

suite "Nil UUID":
  test "Nil array":
    check initUUID() == [0'u8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  test "Nil string value":
    check $initUUID() == "00000000-0000-0000-0000-000000000000"
  test "It is Nil":
    check initUUID().isNil
    check UUID([0'u8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]).isNil
    check not UUID([0'u8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]).isNil
  test "UUID variant":
    check initUUID().variant == ncs
  test "UUID version":
    check initUUID().version == unknown

suite "UUID version 4":
  test "Random UUIDs are not equal":
    check genUUIDv4() != genUUIDv4()
  test "UUID is not Nil":
    check not genUUIDv4().isNil
  test "UUID variant":
    check genUUIDv4().variant == rfc
  test "UUID version":
    check genUUIDv4().version == random_number_based

suite "Namespace":
  test "Namespace string values":
    check $NsDNS    == "6ba7b810-9dad-11d1-80b4-00c04fd430c8"
    check $NsURL    == "6ba7b811-9dad-11d1-80b4-00c04fd430c8"
    check $NsOID    == "6ba7b812-9dad-11d1-80b4-00c04fd430c8"
    check $NsX500DN == "6ba7b814-9dad-11d1-80b4-00c04fd430c8"

template testNameBased(someProc: typed, somVersion: UUIDVersion): untyped =
  test "UUIDs with same namespace are equal":
    check someProc("test", nsDNS) == someProc("test", nsDNS)
    check someProc("test", nsURL) == someProc("test", nsURL)
    check someProc("test", nsOID) == someProc("test", nsOID)
    check someProc("test", nsX500) == someProc("test", nsX500)
  test "UUIDs with different namespaces are not equal":
    check someProc("test", nsOID) != someProc("test", nsX500)
  test "UUID is not Nil":
    check not someProc("test", nsDNS).isNil
    check not someProc("test", nsURL).isNil
    check not someProc("test", nsOID).isNil
    check not someProc("test", nsX500).isNil
  test "UUID variant":
    check someProc("test", nsDNS).variant == rfc
    check someProc("test", nsURL).variant == rfc
    check someProc("test", nsOID).variant == rfc
    check someProc("test", nsX500).variant == rfc
  test "UUID version":
    check someProc("test", nsDNS).version == somVersion
    check someProc("test", nsURL).version == somVersion
    check someProc("test", nsOID).version == somVersion
    check someProc("test", nsX500).version == somVersion

suite "UUID version 3":
  testNameBased(genUUIDv3, name_based_md5)
  test "UUID string verify":
    check $genUUIDv3("test", nsX500) == "d9c53a66-fde2-3d04-b5ad-dce3848df07e"

suite "UUID version 5":
  testNameBased(genUUIDv5, name_based_sha1)
  test "UUID string verify":
    check $genUUIDv5("test", nsX500) == "63a3ab2b-61b8-5b04-ae2f-70d3875c6e97"

suite "To string":
  test "Length":
    check ($initUUID()).len == 36
  test "Dashes":
    check ($initUUID())[8]  == '-'
    check ($initUUID())[13] == '-'
    check ($initUUID())[18] == '-'
    check ($initUUID())[23] == '-'
  test "Chars":
    check validChars($initUUID())
    check validChars($genUUIDv4())
    check validChars($genUUIDv3("test", nsX500))
    check validChars($genUUIDv5("test", nsX500))
    check validChars($NsDNS)
    check validChars($NsDNS)
    check validChars($NsOID)
    check validChars($NsX500DN)

suite "Parse UUID":
  test "Parse constants":
    check parseUUID($initUUID()) == initUUID()
    check parseUUID($NsDNS) == NsDNS
    check parseUUID($NsURL) == NsURL
    check parseUUID($NsOID) == NsOID
    check parseUUID($NsX500DN) == NsX500DN
  test "Parse value":
    check parseUUID("63a3ab2b-61b8-5b04-ae2f-70d3875c6e97") == genUUIDv5("test", nsX500)
  test "Parse braces":
    check parseUUID("{" & $initUUID() & "}") == initUUID()
    check parseUUID("{" & $NsX500DN & "}") == NsX500DN
  test "Parse prefix":
    check parseUUID("urn:uuid:" & $initUUID()) == initUUID()
    check parseUUID("urn:uuid:" & $NsX500DN) == NsX500DN
  test "Parse withoud dashes":
    check parseUUID("00000000000000000000000000000000") == initUUID()
    check parseUUID("6ba7b8149dad11d180b400c04fd430c8") == NsX500DN
  test "Parse version":
    check parseUUID($initUUID()).version == unknown
    check parseUUID($genUUIDv4()).version == random_number_based
    check parseUUID($genUUIDv3("test", nsX500)).version == name_based_md5
    check parseUUID($genUUIDv5("test", nsX500)).version == name_based_sha1
  test "Parse variant":
    check parseUUID($initUUID()).variant == ncs
    check parseUUID($genUUIDv4()).variant == rfc
    check parseUUID($genUUIDv3("test", nsX500)).variant == rfc
    check parseUUID($genUUIDv5("test", nsX500)).variant == rfc
  test "Parse Nil":
    check parseUUID($initUUID()).isNil
  test "Parse case insensitive":
    let s = "63a3ab2b-61b8-5b04-ae2f-70d3875c6e97"
    check parseUUID(s.toLowerAscii()) == parseUUID(s.toUpperAscii())
  test "Invalid length":
    expect ValueError:
      discard parseUUID("")
    expect ValueError:
      discard parseUUID("---")
    expect ValueError:
      discard parseUUID("ZZZ")
  test "Invalid dashes":
    expect ValueError:
      discard parseUUID("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
    expect ValueError:
      discard  parseUUID("AAAAAAAA-AAAAAAAAAAAAAAAAAAAAAAAAAAA")
    expect ValueError:
      discard  parseUUID("AAAAAAAA-AAAA-AAAAAAAAAAAAAAAAAAAAAA")
    expect ValueError:
      discard  parseUUID("AAAAAAAA-AAAA-AAAA-AAAAAAAAAAAAAAAAA")
  test "Invalid braces":
    expect ValueError:
      discard parseUUID("{XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXXX")
    expect ValueError:
      discard parseUUID("XXXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}")
    expect ValueError:
      discard parseUUID("{XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX}")
  test "Invalid chars":
    expect ValueError:
      discard parseUUID("XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX")
    expect ValueError:
      discard parseUUID("{XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}")
    expect ValueError:
      discard parseUUID("urn:uuid:XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX")
    expect ValueError:
      discard parseUUID("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")

suite "Variant":
  test "Get UUID variant":
    var u = initUUID()
    let tests = {
      0x00: ncs,
      0x10: ncs,
      0x20: ncs,
      0x30: ncs,
      0x40: ncs,
      0x50: ncs,
      0x60: ncs,
      0x70: ncs,
      0x80: rfc,
      0x90: rfc,
      0xa0: rfc,
      0xb0: rfc,
      0xc0: microsoft,
      0xd0: microsoft,
      0xe0: reserved,
      0xf0: reserved,
    }
    for v in tests:
      u[8] = byte(v[0])
      check u.variant == v[1]
  test "Set UUID variant":
    var u = initUUID()
    for v in UUIDVariant:
      u.variant = v
      check u.variant == v

suite "Version":
  test "Get UUID version":
    var u = initUUID()
    let tests = {
      0x00: unknown,
      0x10: time_based,
      0x20: dce_security,
      0x30: name_based_md5,
      0x40: random_number_based,
      0x50: name_based_sha1,
      0x60: unknown,
      0x70: unknown,
      0x80: unknown,
      0x90: unknown,
      0xa0: unknown,
      0xb0: unknown,
      0xc0: unknown,
      0xd0: unknown,
      0xe0: unknown,
      0xf0: unknown,
    }
    for v in tests:
      u[6] = byte(v[0])
      check u.version == v[1]
  test "Set UUID version":
    var u = initUUID()
    for v in UUIDVersion:
      u.version = v
      check u.version == v

suite "Comparison":
  test "Test ==":
    check initUUID(1'u8) == initUUID(1'u8)
  test "Test !=":
    check initUUID(0'u8) != initUUID(1'u8)
  test "Test >":
    check initUUID(1'u8) > initUUID(0'u8)
  test "Test <":
    check initUUID(0'u8) < initUUID(1'u8)
  test "Test >=":
    check initUUID(1'u8) >= initUUID(0'u8)
  test "Test <=":
    check initUUID(0'u8) <= initUUID(1'u8)

suite "Hash UUID":
  test "Equal hash":
    check NsX500DN.hash == NsX500DN.hash
    check initUUID(1'u8).hash == initUUID(1'u8).hash
  test "Unequal hash":
    check NsDNS.hash != NsX500DN.hash
    check initUUID(0'u8).hash != initUUID(1'u8).hash

suite "Converters":
  test "toUUID":
    check "00000000000000000000000000000000".toUUID == initUUID()
    check "6ba7b8149dad11d180b400c04fd430c8".toUUID == NsX500DN
  test "toString":
    check initUUID().toString == "00000000-0000-0000-0000-000000000000"
    check NsX500DN.toString == "6ba7b814-9dad-11d1-80b4-00c04fd430c8"
  test "Implicit UUID":
    let u: UUID = "6ba7b814-9dad-11d1-80b4-00c04fd430c8"
    check u == NsX500DN
  test "Implicit string":
    let s: string = NsX500DN
    check s == $NsX500DN
