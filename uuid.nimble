# Package

version       = "1.0.0"
author        = "Artem Klevtsov"
description   = "Generates UUID version 3, 4, 5"
license       = "Apache-2.0"
srcDir        = "src"


# Dependencies

requires "nim >= 1.6.4"


# Tasks

task docs, "Generate documentation":
  exec "nim doc --out:public/index.html src/uuid"

task test, "Run all tests":
  exec "nim compile --run --hints:off tests/testall"

task cov, "Generate coverage report":
  exec "coco --verbose --target='tests/**/test*.nim' --cov='src/' --compiler='--hints:off -d:skipTestsOutput' --report_path='public/coverage'"
