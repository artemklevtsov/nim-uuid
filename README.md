# User agent parser

<!-- badges: start -->

[![GitLab CI Build Status](https://gitlab.com/artemklevtsov/nim-uuid/badges/master/pipeline.svg)](https://gitlab.com/artemklevtsov/nim-uuid/pipelines)
[![coverage report](https://gitlab.com/artemklevtsov/nim-uuid/badges/master/coverage.svg)](https://artemklevtsov.gitlab.io/nim-uuid/coverage)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

<!-- badges: end -->

Nim implementation for universally unique identifiers, simply know as either UUID or GUID (mostly on Windows). A UUID is a 128-bit number used to uniquely identify information in computer systems, such as database table keys, COM interfaces, classes and type libraries, and many others.

## installation

```bash
nimble install https://gitlab.com/artemklevtsov/nim-uuid
```

## Usage

```nim
import uuid

# Nil UUID
assert $initUUID() == "00000000-0000-0000-0000-000000000000"
assert initUUID().isNil
assert initUUID().variant == ncs
assert initUUID().version == unknown

# UUID version 4
assert genUUIDv4() != genUUIDv4()
assert not genUUIDv4().isNil
assert genUUIDv4().variant == rfc_4122
assert genUUIDv4().version == random_number_based

# UUID version 5
assert genUUIDv5("test", nsX500) == genUUIDv5("test", nsX500)
assert not genUUIDv5("test", nsX500).isNil
assert not genUUIDv5("test", nsX500).isNil
assert genUUIDv5("test", nsX500).variant == rfc
assert genUUIDv5("test", nsX500).version == name_based_sha1
```
