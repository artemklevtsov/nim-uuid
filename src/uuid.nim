##[
  Package ``uuid`` generates and inspects UUIDs
  are based on RFC 4122 (https://tools.ietf.org/html/rfc4122).
]##

from std/sysrand import urandom
from std/md5 import MD5Context, MD5Digest, md5Init, md5Update, md5Final
from std/sha1 import Sha1State, SecureHash, newSha1State, update, finalize
from std/hashes import Hash, hash, `!$`
# from std/private/decode_helpers import handleHexChar

type
  UUID* = array[16, byte]
    ## A UUID is a 128 bit (16 byte) Universal Unique IDentifier as defined in RFC 4122.

  UUIDVariant* = enum
    ## UUID variant type. Indicated by a bit pattern in octet 8, marked with N in xxxxxxxx-xxxx-xxxx-Nxxx-xxxxxxxxxxxx.
    ncs  ## NCS backward compatibility. N bit pattern: 0xxx.
    rfc  ## RFC 4122/DCE 1.1. N bit pattern: 10xx.
    microsoft  ## Microsoft Corporation backward compatibility. N bit pattern: 110x.
    reserved  ## Reserved for possible reserved definion. N bit pattern: 111x.

  UUIDVersion* = enum
    ## UUID version type. Indicated by a bit pattern in octet 6, marked with M in xxxxxxxx-xxxx-Mxxx-xxxx-xxxxxxxxxxxx.
    unknown = 0  ## Only possible for nil or invalid uuids.
    time_based = 1  ## The time-based version specified in RFC 4122.
    dce_security = 2  ## DCE Security version, with embedded POSIX UIDs.
    name_based_md5 = 3  ## The name-based version specified in RFS 4122 with MD5 hashing.
    random_number_based = 4  ## The randomly or pseudo-randomly generated version specified in RFS 4122.
    name_based_sha1 = 5  ## The name-based version specified in RFS 4122 with SHA1 hashing.

  UUIDHashAlgo* = enum
    ## Hash algorithm for the name based UUID.
    haMD5  = "MD5"
    haSHA1 = "SHA1"

  UUIDNamespace* = enum
    ## Namespaces
    nsDNS  = "DNS"  ## DNS namespace
    nsURL  = "URL"  ## URL namespace
    nsOID  = "OID"  ## OIDs namespace
    nsX500 = "X500DN"  ## X.500 namespace

const
  NsDNS*: UUID = [
    0x6b'u8, 0xa7, 0xb8, 0x10, 0x9d, 0xad, 0x11, 0xd1,
    0x80,    0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8,
  ] ## Name string is a DNS.

  NsURL*: UUID = [
    0x6b'u8, 0xa7, 0xb8, 0x11, 0x9d, 0xad, 0x11, 0xd1,
    0x80,    0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8,
  ] ## Name string is a URL.

  NsOID*: UUID = [
    0x6b'u8, 0xa7, 0xb8, 0x12, 0x9d, 0xad, 0x11, 0xd1,
    0x80,    0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8,
  ] ## Name string is an ISO OID.

  NsX500DN*: UUID = [
    0x6b'u8, 0xa7, 0xb8, 0x14, 0x9d, 0xad, 0x11, 0xd1,
    0x80,    0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8,
  ] ## Name string is an X.500 DN.

func variant*(u: UUID): UUIDVariant =
  ## Get UUID variant.
  runnableExamples:
    doAssert initUUID().variant == ncs
    doAssert genUUIDv4().variant == rfc
    doAssert genUUIDv3("test", NsX500DN).variant == rfc
    doAssert genUUIDv5("test", NsX500DN).variant == rfc

  let x = u[8]
  if   (x and 0x80) == 0x00: # 0b0xxxxxxx
    return ncs
  elif (x and 0xC0) == 0x80: # 0b10xxxxxx
    return rfc
  elif (x and 0xE0) == 0xC0: # 0b110xxxxx
    return microsoft
  elif (x and 0xE0) == 0xE0: # 0b1110xxxx
    return reserved

func `variant=`*(u: var UUID, v: UUIDVariant) =
  ## Set UUID variant.
  case v
  of ncs:       # not 0x80
    u[8] = u[8] and 0x7F or 0x00
  of rfc:       # not 0xC0
    u[8] = u[8] and 0x3F or 0x80
  of microsoft: # not 0xE0
    u[8] = u[8] and 0x1F or 0xC0
  of reserved:  # not 0xE0
    u[8] = u[8] and 0x1F or 0xE0

func version*(u: UUID): UUIDVersion =
  ## Get UUID version.
  runnableExamples:
    doAssert initUUID().version == unknown
    doAssert genUUIDv4().version == random_number_based
    doAssert genUUIDv3("test", NsX500DN).version == name_based_md5
    doAssert genUUIDv5("test", NsX500DN).version == name_based_sha1

  # result = UUIDVersion((u[6] and 0xF0) shr 4) # out of range
  let x = u[6] and 0xF0 # omit right 4 bits
  result = case x
  of 0x10: time_based
  of 0x20: dce_security
  of 0x30: name_based_md5
  of 0x40: random_number_based
  of 0x50: name_based_sha1
  else: unknown

func `version=`*(u: var UUID, v: UUIDVersion) =
  ## Set UUID version.
  u[6] = u[6] and 0x0F or (ord(v) shl 4).byte

func hash*(u: UUID): Hash =
  ## Get UUID hash.
  result = !$ hash[byte](u)

func isNil*(u: UUID): bool =
  ## Check UUID is Nil.
  runnableExamples:
    doAssert initUUID().isNil
    doAssert not genUUIDv4().isNil

  result = true
  for i in u:
    if i != 0:
      return false

func cmp*(lhs, rhs: UUID): int =
  ## Compare UUIDs.
  cmpMem(unsafeAddr lhs[0], unsafeAddr rhs[0], sizeof(UUID))

func `==`*(lhs, rhs: UUID): bool =
  cmp(lhs, rhs) == 0

func `!=`*(lhs, rhs: UUID): bool =
  not (lhs == rhs)

func `>`*(lhs, rhs: UUID): bool =
  cmp(lhs, rhs) > 0

func `<`*(lhs, rhs: UUID): bool =
  cmp(lhs, rhs) < 0

func `>=`*(lhs, rhs: UUID): bool =
  not (lhs < rhs)

func `<=`*(lhs, rhs: UUID): bool =
  not (lhs > rhs)

func initUUID*(): UUID =
  ## Get Nil UUID. 00000000-0000-0000-0000-000000000000.
  result

func initUUID*(a: openArray[byte]): UUID =
  ## Init UUID with openArray.
  runnableExamples:
    import sequtils
    let s = repeat(255'u8, 16)
    doAssert initUUID(s).toSeq == s

  assert a.len == sizeof(UUID)
  copyMem(addr result[0], unsafeAddr a[0], sizeof(UUID))

func initUUID*(b: byte): UUID =
  ## Init UUID with byte.
  runnableExamples:
    import sequtils
    let s = repeat(255'u8, 16)
    doAssert initUUID(255'u8).toSeq == s

  var a {.noinit.}: UUID
  for i in 0 ..< sizeof(UUID):
    a[i] = b
  result = a

template getNamespace(ns: UUIDNamespace): UUID =
  case ns
  of nsDNS: NsDNS
  of nsURL: NsURL
  of nsOID: NsOID
  of nsX500: NsX500DN

proc genUUIDv4*(): UUID =
  ## Generate UUID version 4.
  runnableExamples:
    # not equal Nil UUID
    doAssert genUUIDv4() != initUUID()
    # two random UUIDs are not equal
    doAssert genUUIDv4() != genUUIDv4()

  if not urandom(result):
    raise newException(OSError, "Can not generate random UUID.")
  result.version = random_number_based
  result.variant = rfc

func genUUIDv3*(name: string, namespace: UUID): UUID =
  ## Generate UUID version 3.
  runnableExamples:
    doAssert genUUIDv3("test", NsX500DN) == genUUIDv3("test", NsX500DN)
    doAssert $genUUIDv3("test", NsX500DN) == "d9c53a66-fde2-3d04-b5ad-dce3848df07e"

  var ctx: MD5Context
  md5Init(ctx)
  md5Update(ctx, cast[cstring](unsafeAddr namespace[0]), sizeof(UUID))
  md5Update(ctx, cstring(name), name.len)
  var digest: MD5Digest
  md5Final(ctx, digest)
  copyMem(addr result[0], addr digest[0], sizeof(UUID))
  result.version = name_based_md5
  result.variant = rfc

func genUUIDv3*(name: string, namespace: UUIDNamespace): UUID =
  ## Generate UUID version 3.
  genUUIDv3(name, getNamespace(namespace))

func genUUIDv5*(name: string, namespace: UUID): UUID =
  ## Generate UUID version 5.
  runnableExamples:
    doAssert genUUIDv5("test", NsX500DN) == genUUIDv5("test", NsX500DN)
    doAssert $genUUIDv5("test", NsX500DN) == "63a3ab2b-61b8-5b04-ae2f-70d3875c6e97"

  var ctx = newSha1State()
  update(ctx, cast[array[sizeof(UUID), char]](namespace))
  update(ctx, cast[seq[char]](name))
  var digest = finalize(ctx)
  copyMem(addr result[0], addr digest[0], sizeof(UUID))
  result.version = name_based_sha1
  result.variant = rfc

func genUUIDv5*(name: string, namespace: UUIDNamespace): UUID =
  ## Generate UUID version 5.
  genUUIDv5(name, getNamespace(namespace))

proc genUUID*(): UUID =
  ## Alias to ``genUUIDv4``.
  genUUIDv4()

func genUUID*(name: string, namespace: UUID, hashAlgo: UUIDHashAlgo = haSHA1): UUID =
  ## Generate name based UUID.
  case hashAlgo
  of haMD5: genUUIDv3(name, namespace)
  of haSHA1: genUUIDv5(name, namespace)

func genUUID*(name: string, namespace: UUIDNamespace, hashAlgo: UUIDHashAlgo = haSHA1): UUID =
  ## Generate name based UUID.
  genUUID(name, getNamespace(namespace), hashAlgo)

func processByte(x: byte): char {.inline.} =
  if x <= 9:
    result = char(ord('0') + x)
  else:
    result = char(ord('a') + (x - 10))

func `$`*(u: UUID): string =
  ## UUID string representation.
  runnableExamples:
    doAssert $initUUID() == "00000000-0000-0000-0000-000000000000"
    doAssert $NsDNS      == "6ba7b810-9dad-11d1-80b4-00c04fd430c8"
    doAssert $NsURL      == "6ba7b811-9dad-11d1-80b4-00c04fd430c8"
    doAssert $NsOID      == "6ba7b812-9dad-11d1-80b4-00c04fd430c8"
    doAssert $NsX500DN   == "6ba7b814-9dad-11d1-80b4-00c04fd430c8"

  result = newStringOfCap(36)
  for i, v in u:
    # result.add v.toHex(2)
    result.add processByte(v shr 4)    # get  low 4 bits: 0b0000xxxx
    result.add processByte(v and 0x0F) # get high 4 bits: 0bxxxx0000
    if i == 3 or i == 5 or i == 7 or i == 9:
      result.add '-'

func parseError(msg: string) =
  raise ValueError.newException:
    "Parse UUID failed: " & msg

template checkDashes(s: string, pos: Positive = 0): bool =
  s[pos + 8] != '-' or
  s[pos + 13] != '-' or
  s[pos + 18] != '-' or
  s[pos + 23] != '-'

func checkUuidLen(s: string): tuple[startPos, endPos: int, skipDashes: bool] =
  var failed = false
  case s.len
  of 36 - 4: # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    result = (0, 32, false)
  of 36:     # XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
    result = (0, 36, true)
    failed = checkDashes(s, result.startPos)
  of 36 + 2: # {XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}
    result = (1, 37, true)
    failed = s[0] != '{' or s[^1] != '}' or checkDashes(s, result.startPos)
  of 36 + 9: # urn:uuid:XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
    result = (9, 45, true)
    failed = s[0 .. 8] != "urn:uuid:" or checkDashes(s, result.startPos)
  else:
    failed = true
  if failed:
    parseError("invalid UUID format")

func parocessChar(c: char): byte =
  # Based on decode_helpers.handleHexChar
  case c
  of '0' .. '9': result = byte(c) - byte('0')
  of 'a' .. 'f': result = byte(c) - byte('a') + 10
  of 'A' .. 'F': result = byte(c) - byte('A') + 10
  else: parseError("invalid UUID format")

func parseUUID*(s: string): UUID =
  ## Parse decodes s into a UUID or returns an error.
  ##
  ## Both the standard UUID forms of xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
  ## and urn:uuid:xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx are decoded as well
  ## as the Microsoft encoding {xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}
  ## and the raw hex encoding: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
  runnableExamples:
    doAssert parseUUID("00000000-0000-0000-0000-000000000000") == initUUID()
    doAssert parseUUID("6ba7b8149dad11d180b400c04fd430c8") == NsX500DN
    doAssert parseUUID("6ba7b814-9dad-11d1-80b4-00c04fd430c8") == NsX500DN
    doAssert parseUUID("{6ba7b814-9dad-11d1-80b4-00c04fd430c8}") == NsX500DN
    doAssert parseUUID("urn:uuid:6ba7b814-9dad-11d1-80b4-00c04fd430c8") == NsX500DN

  var (pos, n, skip) = checkUuidLen(s)
  var i = 0
  while pos < n - 1:
    if s[pos] == '-' and skip: # skip dashes
      inc pos
    else:
      result[i] = (parocessChar(s[pos]) shl 4) or parocessChar(s[pos + 1])
      inc pos, 2
      inc i

converter toUUID*(s: string): UUID =
  ## Convert string to UUID.
  result = parseUUID(s)

converter toString*(u: UUID): string =
  ## Convert UUID to string.
  result = $u
